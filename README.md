# traderlo-upcloud-service

1. Install nodejs and npm

```
sudo apt update
sudo apt install nodejs
sudo apt install npm
nodejs -v
```

2. Install LoopBack 4 CLI

```
npm i -g @loopback/cli
```

3. Run the app


```
npm start
http://127.0.0.1:3000/ping
```